#include <thread>
#include <sstream>
#include <iostream>
#include <exception>

void ThrowException() {
    throw std::runtime_error("Catch this!");
}

int main() {
    std::thread worker(ThrowException);

    worker.join();
    return 0;
}
